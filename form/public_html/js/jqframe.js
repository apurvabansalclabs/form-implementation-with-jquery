/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Function to be performed when document loads
$(document).ready(function()
{
    //Prevents future date to be selected for date of birth
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    month = month + 1;
    if (day < 10)
    {
        day = '0' + day;
    }
    if (month < 10)
    {
        month = '0' + month;
    }
    var currentdate = (year + "-" + month + "-" + day);
    $("#dob").attr('max', currentdate);

    //Code to format phone no. to contain "-"
    $("#phone").keyup(function(e)
    {
        if (e.keyCode !== 8) {
            if ($(this).val().length === 3) {
                $(this).val($(this).val() + "-");
            } else if ($(this).val().length === 7) {
                $(this).val($(this).val() + "-");
            }
        }
    });

//Sets focus color for all textboxes
    $("input").focus(function()
    {
        $(this).css("background-color", "lightgrey");
    });

//  Sets blur color for all textboxes
    $("input").blur(function()
    {
        $(this).css("background-color", "ghostwhite");
    });

//  Functions to be performed on clicking the submit button 
    $("#button-submit").click(function()
    {
        event.preventDefault();


//Code to restrict null value and numbers in first name
        valid = true;
        if ($("#f-name").val() === "" || null)
        {
            $("#err-f-name-empty").css("color", "red");
            $("#err-f-name-empty").html("Please enter your first name");
            valid = false;
        }
        else
        {
            var reg = /^[a-zA-Z]*$/;
            if (!(reg.test($("#f-name").val())))
            {
                $("#err-f-name-wrong").css("color", "red");
                $("#err-f-name-wrong").html("First name can only contain alphabets");
                valid = false;
            }
        }

//Code to restrict null value and numbers in last name
        valid = true;
        if ($("#l-name").val() === "" || null)
        {
            $("#err-l-name-empty").css("color", "red");
            $("#err-l-name-empty").html("Please enter your last name");
            valid = false;
        }
        else
        {
            var reg = /^[a-zA-Z]*$/;
            if (!(reg.test($("#l-name").val())))
            {
                $("#err-l-name-wrong").css("color", "red");
                $("#err-l-name-wrong").html("Last name can only contain alphabets");
                valid = false;
            }
        }

//Code to check that one radio button of gender is selected
        if (!$('input[name="gender"]:checked').val())
        {
            $("#err-gender-empty").css("color", "red");
            $("#err-gender-empty").html("Please select gender");
            valid = false;
        }

//Code to check date of birth should not be null
        if ($("#dob").val() === "" || null)
        {
            $("#err-dob-empty").css("color", "red");
            $("#err-dob-empty").html("Please select Date of Birth");
            valid = false;
        }


//Code to check phone no. should not be empty 
        if ($("#phone").val() === "" || null)
        {
            $("#err-phone-empty").css("color", "red");
            $("#err-phone-empty").html("Please enter phone no.");
            valid = false;
        }

//Code to restrict alphabets and special characters in phone numbers
        else
        {
            var tel = /^[0-9-]*$/;
            if (!tel.test($("#phone").val()))
            {
                $("#err-phone-wrong").css("color", "red");
                $("#err-phone-wrong").html("Phone no. can only contain no. numbers");
                valid = false;
            }

//Code to check length of phone no.
            if ($("#phone").val().length > 12)

            {
                $("#err-phone-length").css("color", "red");
                $("#err-phone-length").html("Phone no. should be of 10 digits. ");
                valid = false;
            }
        }

//Code to restrict null or invalid email
        if ($("#email").val() === "" || null)
        {
            $("#err-email-empty").css("color", "red");
            $("#err-email-empty").html("Please enter email");
            valid = false;
        }
        else
        {
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!(reg.test($("#email").val())))
            {
                $("#err-email-invalid").css("color", "red");
                $("#err-email-invalid").html("E-mail invalid");
                valid = false;
            }
        }

// Code to restrict empty password and no. of characters in password
        if ($("#pass").val() === "" || null)
        {
            $("#err-pass-empty").css("color", "red");
            $("#err-pass-empty").html("Please enter password");
            valid = false;
        }
        else {
            if ($("#pass").val().length <= 6)
            {
                $("#err-pass-length").css("color", "red");
                $("#err-pass-length").html("Password length should be of minimum 6 characters");
                valid = false;
            }
        }

//Code to restrict emty confirm password field and to match password and confirm password fields      
        if ($("#cpass").val() === "" || null)
        {
            $("#err-cpass-empty").css("color", "red");
            $("#err-cpass-empty").html("Please confirm password");
            valid = false;
        }
        else
        {
            if ($("#pass").val() !== $("#cpass").val())
            {
                $("#err-cpass-match").css("color", "red");
                $("#err-cpass-match").html("Password does not match");
            }
        }

//Code to check atleast one checkbox is selected
        if ($('input[type=checkbox]:checked').length === 0)
        {
            $("#err-hobby-empty").css("color", "red");
            $("#err-hobby-empty").html("Please select hobby");
            valid = false;
        }

// Code to display submit seccessful message and hide error msgs
        if (valid === true)
        {
            $("#submit-success").html("Form submitted successfully");
            $("span").hide();
        }
    });
});

